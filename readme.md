# Nechifor Machine

A Vagrant configuration for my [nechifor.net](http://nechifor.net) domain.

## Usage

Get the image created by building [nechifor-web][nechifor-web].

    cp <some place>/nechifor-web-1.tar.xz synced/nechifor-web-1.tar.xz

Start the server:

    vagrant up

## TODO

- Use cgit instead of GitLab.

## License

ISC

[nechifor-web]: https://github.com/paul-nechifor/nechifor-web
